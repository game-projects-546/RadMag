// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CoreMinimal.h"
#include "Containers/StaticArray.h"

namespace GameModule
{
    struct CHexData
    {
        FIntVector CubeCoordinate;
        float HeightValue;

        CHexData() = default;
    };
}
