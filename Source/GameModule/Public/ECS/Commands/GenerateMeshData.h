// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "World/BaseSystem.h"

namespace GameModule
{
    using namespace UnrealEntt;

    struct FGenerateMeshData final : public FBaseSystem
    {
    public:
        virtual ~FGenerateMeshData() = default;

        virtual ESystemStatus Tick(FEnttData &Data, float DeltaTime) override;
    };
}