// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "World/BaseSystem.h"
#include "Actors/MapActor.h"
#include "Engine/World.h"

namespace GameModule
{
    using namespace UnrealEntt;

    struct FCreateMapActor final : public FBaseSystem
    {
    protected:
        UWorld *World;
        TSubclassOf<AMapActor> MapActorClass;

    public:
        FCreateMapActor() = delete;
        FCreateMapActor(UWorld *World, TSubclassOf<AMapActor> MapActorClass)
            : World(World), MapActorClass(MapActorClass) {};

        virtual ~FCreateMapActor() = default;

        virtual ESystemStatus Tick(FEnttData &Data, float DeltaTime) override;
    };
}