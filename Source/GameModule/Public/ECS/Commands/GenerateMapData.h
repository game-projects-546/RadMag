// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "World/BaseSystem.h"
#include "Data/MapDataInput.h"

class AMapActor;

namespace GameModule
{
    using namespace UnrealEntt;

    struct FGenerateMapData final : public FBaseSystem
    {
    protected:
        FMapDataInput MapDataInput;

        float MinHeight = TNumericLimits<float>::Max();
        float MaxHeight = TNumericLimits<float>::Min();
        TArray<float> HeightNoise;

    public:
        FGenerateMapData(FMapDataInput &MapDataInput)
            : MapDataInput(MapDataInput){};

        virtual ~FGenerateMapData() = default;

        virtual ESystemStatus Tick(FEnttData &Data, float DeltaTime) override;

    protected:

        void GenerateNoise();
    };
}