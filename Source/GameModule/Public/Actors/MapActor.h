// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "MapActor.generated.h"

UCLASS()
class GAMEMODULE_API AMapActor : public AActor
{
	GENERATED_BODY()

protected:

	UPROPERTY()
	UProceduralMeshComponent *MeshComponent;

	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface *MeshMaterial;

public:
	AMapActor();
	virtual void Tick(float DeltaTime) override;

protected:
	virtual void BeginPlay() override;

public:
	void Update(const TArray<FVector> &Vertices, const TArray<int32> &Triangles, const TArray<FLinearColor> &VertexColors);
};
