// Fill out your copyright notice in the Description page of Project Settings.

#include "ECS/Commands/CreateMapActor.h"
#include "ECS/ECSHeader.h"

namespace GameModule
{
    ESystemStatus FCreateMapActor::Tick(FEnttData &Data, float DeltaTime)
    {
        auto MapActor = World->SpawnActor<AMapActor>(MapActorClass, FVector::ZeroVector, FRotator::ZeroRotator, FActorSpawnParameters());
        auto &MapData = Data.GetResource<FMapData>();
        MapActor->Update(MapData.Vertices, MapData.Triangles, MapData.VertexColors);

        return ESystemStatus::Stop;
    };
}