// Fill out your copyright notice in the Description page of Project Settings.

#include "ECS/Commands/GenerateMapData.h"
#include "Actors/MapActor.h"
#include "ECS/ECSHeader.h"
#include "UnrealEnttHeader.h"
#include "FastNoiseLite.h"

namespace GameModule
{
    ESystemStatus FGenerateMapData::Tick(FEnttData &Data, float DeltaTime)
    {
        GenerateNoise();
        FMapData MapData;
        MapData.MapLength = MapDataInput.MapLength;
        MapData.MapWidth = MapDataInput.MapWidth;
        MapData.DeepWater = MapDataInput.DeepWater;
        MapData.ShallowWater = MapDataInput.ShallowWater;
        MapData.Sand = MapDataInput.Sand;
        MapData.Grass = MapDataInput.Grass;
        MapData.Forest = MapDataInput.Forest;
        MapData.Rock = MapDataInput.Rock;
        MapData.Snow = MapDataInput.Snow;

        for (SIZE_T Y = 0; Y < MapDataInput.MapWidth; ++Y)
        {
            for (SIZE_T X = 0; X < MapDataInput.MapLength; ++X)
            {
                const auto Entity = Data.CreateEntity(EHex{});
                auto &HexData = Data.Get(Entity, entt::type_list<CHexData>{});
                HexData.CubeCoordinate = FMapData::ConvertToCubeCoordinate(FIntVector2(X, Y));
                HexData.HeightValue = (HeightNoise[MapDataInput.MapLength * Y + X] - MinHeight) / (MaxHeight - MinHeight);
                MapData.Hexes.Add(Entity);
            }
        }

        Data.CreateResource(MapData);

        return ESystemStatus::Stop;
    };

    void FGenerateMapData::GenerateNoise()
    {
        FastNoiseLite Noise(MapDataInput.Seed);
        Noise.SetFractalType(FastNoiseLite::FractalType_DomainWarpProgressive);
        Noise.SetNoiseType(FastNoiseLite::NoiseType_OpenSimplex2S);
        Noise.SetFractalOctaves(MapDataInput.Octaves);
        Noise.SetFrequency(MapDataInput.Frequency);

        for (SIZE_T Y = 0; Y < MapDataInput.MapWidth; ++Y)
        {
            for (SIZE_T X = 0; X < MapDataInput.MapLength; ++X)
            {
                float XOne = 0, XTwo = 1;
                float YOne = 0, YTwo = 1;
                float dX = XTwo - XOne;
                float dY = YTwo - YOne;

                float S = X / (float)MapDataInput.MapLength;
                float T = Y / (float)MapDataInput.MapWidth;

                float nX = XOne + FMath::Cos(S * 2 * PI) * dX / (2 * PI);
                float nY = XOne + FMath::Sin(S * 2 * PI) * dX / (2 * PI);
                float nZ = T;

                float Value = Noise.GetNoise(nX, nY, nZ);

                if (Value > MaxHeight)
                    MaxHeight = Value;
                if (Value < MinHeight)
                    MinHeight = Value;

                HeightNoise.Add(Value);
            }
        }
    };
}