﻿#include "GameModule/Public/GameModule.h"
#include "Modules/ModuleManager.h"
#include "Modules/ModuleInterface.h"

IMPLEMENT_GAME_MODULE(FGameModule, GameModule);

DEFINE_LOG_CATEGORY(GameModule);
 
#define LOCTEXT_NAMESPACE "GameModule"
 
void FGameModule::StartupModule()
{
	UE_LOG(GameModule, Warning, TEXT("Game module has started!"));
}
 
void FGameModule::ShutdownModule()
{
	UE_LOG(GameModule, Warning, TEXT("Game module has shut down"));
}
 
#undef LOCTEXT_NAMESPACE