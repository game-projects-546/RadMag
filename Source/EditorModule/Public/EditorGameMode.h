// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EditorGameMode.generated.h"

class UGameData;
class AMapActor;
namespace UnrealEntt
{
	class FEnttWorld;
}

/**
 * 
 */
UCLASS()
class EDITORMODULE_API AEditorGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditDefaultsOnly)
	UGameData *GameData;

protected:
	virtual void BeginPlay() override;
};
