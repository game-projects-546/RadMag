// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealEnttSubsystem.h"
#include "Engine/World.h"

void UUnrealEnttSubsystem::Initialize(FSubsystemCollectionBase &Collection)
{
	UE_LOG(LogTemp, Warning, TEXT("UnrealEnttSubsystem has started!"));
	World = MakeUnique<UnrealEntt::FEnttWorld>();
	OnTickDelegate = FTickerDelegate::CreateUObject(this, &UUnrealEnttSubsystem::Tick);
	OnTickHandle = FTicker::GetCoreTicker().AddTicker(OnTickDelegate);
	Super::Initialize(Collection);
}

void UUnrealEnttSubsystem::Deinitialize()
{
	FTicker::GetCoreTicker().RemoveTicker(OnTickHandle);
	UE_LOG(LogTemp, Warning, TEXT("UnrealEnttSubsystem has shut down!"));
	Super::Deinitialize();
}

bool UUnrealEnttSubsystem::Tick(float DeltaTime) const
{
	if (GetWorld() && !GetWorld()->IsPaused() && World.IsValid())
		World->Tick(DeltaTime);
	return true;
}

UnrealEntt::FEnttWorld *UUnrealEnttSubsystem::GetEnttWorld() const
{
	return World.Get();
}