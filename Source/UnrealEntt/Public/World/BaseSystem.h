// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

namespace UnrealEntt
{
    class FEnttData;
    
	enum class ESystemStatus
	{
		Repeat,
		Stop,
	};
	
	struct FBaseSystem
	{
		virtual ~FBaseSystem() = default;
		virtual ESystemStatus Tick(FEnttData& Data, float DeltaTime) = 0;
	};
}
