// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "EnttData.h"
#include "BaseSystem.h"

namespace UnrealEntt
{
    class FEnttWorld
    {
    private:
        TUniquePtr<FEnttData> Data;
        TArray<TUniquePtr<FBaseSystem>> Systems;
        TArray<TUniquePtr<FBaseSystem>> Commands;

    public:
        FEnttWorld()
        {
            Data = MakeUnique<FEnttData>();
        }

        void AddSystem(TUniquePtr<FBaseSystem> System)
        {
            Systems.Add(MoveTemp(System));
        }

        void AddCommands(TUniquePtr<FBaseSystem> Command)
        {
            Commands.Add(MoveTemp(Command));
        }

        bool Tick(float DeltaTime)
        {
            check(Data.IsValid());

            if (Commands.Num() > 0)
            {
                for (SIZE_T Index = 0; Index < Commands.Num(); ++Index)
                {
                    Commands[Index]->Tick(*Data.Get(), DeltaTime);
                }

                Commands.Empty();
            }

            for (SIZE_T Index = 0; Index < Systems.Num(); ++Index)
            {
                const auto Status = Systems[Index]->Tick(*Data.Get(), DeltaTime);
                if (Status == ESystemStatus::Stop)
                {
                    Systems.RemoveAt(Index);
                }
            }
            return true;
        }
    };
}
