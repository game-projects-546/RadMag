// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "entt.hpp"

namespace UnrealEntt
{
    using EEntity = entt::entity;

    class FEnttData
    {
    protected:
        entt::registry ResourceStorage;
        entt::registry EntityStorage;

    public:

        FEnttData() = default;

        template <typename... Comp, template<typename...> typename List>
		EEntity CreateEntity(List<Comp...>)
		{
			const auto Entity = EntityStorage.create();
			(EntityStorage.emplace<Comp>(Entity), ...);
			return Entity;
		}

        template <typename... Comp, template<typename...> typename List>
		decltype(auto) Get(EEntity Entity, List<Comp...>)
		{
            return EntityStorage.get<Comp...>(Entity);
		}

        template <typename... Comp, template<typename...> typename List>
		decltype(auto) Get(EEntity Entity, List<Comp...>) const
		{
            return EntityStorage.get<Comp const...>(Entity);
		}


        template <typename... Comp, typename... Exclude, template<typename...> typename List>
		decltype(auto) GetView(List<Comp...>, entt::exclude_t<Exclude...> ExcludeList = {})
		{
			return EntityStorage.view<Comp...>(ExcludeList);
		}

		template <typename... Comp, typename... Exclude, template<typename...> typename List>
		decltype(auto) GetView(List<Comp...>, entt::exclude_t<Exclude...> ExcludeList = {}) const
		{
			return EntityStorage.view<Comp const...>(ExcludeList);
		}

        template<typename ResourceType>
        void CreateResource(ResourceType Resource)
        {
            auto Vi = ResourceStorage.view<ResourceType>();
            check(Vi.begin() == Vi.end());

            const auto Id = ResourceStorage.create();
            ResourceStorage.emplace<ResourceType>(Id, Resource);
        }

        template <typename FResource>
		decltype(auto) GetResource()
		{
			auto Vi = ResourceStorage.view<FResource>();
			check(Vi.begin() != Vi.end());
			return Vi.template get<FResource>(*Vi.begin());
		}
    };
};